# nRF52840 Coordinate Beacon

A BLE Beacon on the nRF52840 development kit to transmit GPS coordinates received through PC console input.

## Usage
Write a string of up to 14 characters to the nRF52840 console to have it advertised by the board. On Linux, you can do this with
	
	printf "$(string)\r\n" > /dev/ttyACM0

 - Replace `ttyACM0`  with the port in your case, if it is different.

Alternatively, you can use the LTE Link monitor in nRF Connect, or any other terminal emulator. To observe the advertisement I used [nRF Connect](https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp) ([iOS](https://apps.apple.com/us/app/nrf-connect-for-mobile/id1054362403)), but any other BLE scanner should work.
The bluetooth device name is `distinctive bluetooth device`. 

Since there is no protocol for advertising GPS data, the coordinates (or whatever other input) are transmitted as plaintext. On nRF Connect, viewing the advertising data in plaintext is supported, but not enabled by default. It can be enabled by selecting the device `distinctive bluetooth device` from the list, tapping `Service data` and then selecting `Text (UTF-8)`. The initial advertised string is `0000`.

## Building and installation
This repository only includes the source and configuration files of the project, to avoid clutter. To build or flash it, you will need to use [West](https://docs.zephyrproject.org/2.5.0/guides/west/index.html) or some other tool that can flash .hex files.

### Building
Clone the repository and `cd` into it:

    git clone https://gitlab.com/UpDownUp/gpsbeacon.git
    cd gpsbeacon

Prepare a West instance:

    #this will take a while
    west init
    #this too
    west update

When `west update` is complete, run 

    west build -b nrf52840dk_nrf52840
    west flash
to build and flash it.

### Install prebuilt image
Alternatively, to flash the included precompiled build, run 

    west flash --hex-file build/zephyr/zephyr.hex

## Specifications

*	GPS coordinates should be given to the beacon via command from PC 
	*	Fulfilled in `src/main.c:88`
*	Bluetooth beacon should have a distinctive bluetooth device name
	*	Fulfilled in `prj.conf:8`
*	GPS coordinates should be transmitted as advertising data
	*	Fulfilled in `src/main.c:90`

Note: Received input is not validated, and input strings that do not contain coordinates will still be advertised.

## Credits

This program was built on top of the [getline](https://github.com/zephyrproject-rtos/zephyr/tree/master/samples/subsys/console/getline) and [beacon](https://github.com/zephyrproject-rtos/zephyr/tree/master/samples/bluetooth/beacon) sample programs from Zephyr. Since Zephyr's Apache 2.0 license mandates that derivative works have a copy of it, this project is also licensed under Apache 2.0.

