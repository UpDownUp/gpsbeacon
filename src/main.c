//gpsbeacon
//Sherif elSherif (updownup)
//Apache 2.0 License
//Built on top of existing Zephyr samples (subsys/console/getline and bluetooth/beacon)
#include <zephyr/types.h>
#include <stddef.h>
#include <sys/printk.h>
#include <sys/util.h>

#include <string.h>
#include <zephyr.h>
#include <console/console.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>

static const unsigned char uuid_s[2] = {0xFA, 0x20}; //uuid, to be prefixed to advertising string
static unsigned char coord[16] = {0xFA,0x20, '0','0','0','0'};	//coordinate data, cannot be longer than 16 bytes, initialized as a bunch of zeros

static struct bt_data gps_ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, BT_LE_AD_NO_BREDR),	//Do not use classic bluetooth
	BT_DATA_BYTES(BT_DATA_UUID16_ALL, 0xFA, 0x20),		//Random unreserved UUID 
//	Reserved UUIDs: https://specificationrefs.bluetooth.com/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
	BT_DATA(BT_DATA_SVC_DATA16, coord, sizeof(coord))	//Service data is the contents of coord
};

static struct bt_data scandata[] = { //bluetooth scan response data
	BT_DATA(BT_DATA_NAME_COMPLETE, CONFIG_BT_DEVICE_NAME, (sizeof(CONFIG_BT_DEVICE_NAME) - 1)),
};

static void bt_ready(int err) {	//input: error code from bt_enable	
	//quit immediately if error from previous invocation
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}
	printk("Bluetooth initialized\n");

	//Addressing
	char addr_s[BT_ADDR_LE_STR_LEN];	//buffer for string address
	bt_addr_le_t addr = {0};			//buffer for binary address
	size_t count = 1;					//size of addr

	bt_id_get(&addr, &count);			//configure address
	bt_addr_le_to_str(&addr, addr_s, sizeof(addr_s));	//write address as string

	/* Start advertising */
	err = bt_le_adv_start(BT_LE_ADV_NCONN_IDENTITY, gps_ad, ARRAY_SIZE(gps_ad),
			      scandata, ARRAY_SIZE(scandata));
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	printk("Beacon started, advertising as %s\n", addr_s);
}

void main(void) {
	int err;

	printk("Starting Beacon Demo\n");

	//Initialize Bluetooth
	err = bt_enable(bt_ready);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
	}
	
	//start console, get input
	console_getline_init();
	printk("Enter the coordinates to be advertised (format: xx.xxx,xx.xxx)\n");

	//keep broadcasting the contents of console input forever
	while (1) {
		char *input = console_getline();

		printk("input: %s\n", input);
		//advertising data is sent and can be read as UTF-8 plaintext, so no need to process it
		strcpy(coord, uuid_s);
		strncpy(coord+2, input, 16);

		bt_le_adv_update_data(gps_ad, ARRAY_SIZE(gps_ad),
			  				scandata, ARRAY_SIZE(scandata));

	}

}
